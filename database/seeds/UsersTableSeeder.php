<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'yoram',
                    'email' => 'a@a.com',
                    'role' => 'owner',
                    'password' =>Hash::make('12345678'),
                    'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                 'name' => 'eli',
                 'email' => 'b@b.com',
                 'role' => 'owner',
                 'password' =>Hash::make('12345678'),
                 'created_at' => date('Y-m-d G:i:s'),
                ],
                
                
            ]);
        }
}
