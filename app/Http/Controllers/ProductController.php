<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    
     public function index()
    {
        if (Gate::denies('owner')){  
            if (Gate::denies('customer')) {
                abort(403,"Are you a hacker or what?");} }  
        
            $product= Product::all();
            return view('products.index',['products' => $product]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('owner')) {
            abort(403,"Sorry you are not allowed to create products..");
        }
 
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product= new Product();
        $id =Auth::id();//the idea of the current user
        $product->user_id = $id;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->status = 0;
        $product->save();
        return redirect('products');
 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);  
        if (Gate::denies('owner')) {
            abort(403,"You are not allowed to edit todos..");}
        return view('products.edit', compact('product'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $id=Auth::id();
        if($request->ajax()){
            $product->user_id=$id;
            $boss = DB::table('users')->where('id',$id)->first();
            $username = $boss->name;
            $product->username = $username;
            $product->update($request->except(['_token']));
            return Response::json(array('result' => 'success','status' => $request->status ), 200);    
         } else {
                       
            $product->update($request->except(['_token']));     
        return redirect('products');
         }     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findorfail($id);
        if (Gate::denies('owner')) {
            abort(403,"You are not allowed to delete products..");}
        $product->delete();
        return redirect('products');
    }
}
