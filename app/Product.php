<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name','price','user_id','status','username'
   ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
 
}
