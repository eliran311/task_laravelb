@extends('layouts.app')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<table >
<thead><tr>
<th > Name</th>
<th > Price</th>




</tr> </thead>
<tbody>@foreach($products as $product)

<td > {{$product->name}}</td>
<td > {{$product->price}}</td>
@can('owner')<td > {{$product->username}}</td>@endcan('owner')   
@can('owner')<td><a href="{{route('products.edit', $product->id)}}">Edit</a></td> @endcan('owner')

@can('owner')<td> 
<form method = 'post' action = "{{action('ProductController@destroy',$product->id)}}" >
@csrf   
@method('DELETE')    
<div class = "form-group">    
    <input type = "submit" class = "form-control" name = "submit" value = "Delete">
</div>
</form></td> @endcan('owner')
<td>
@if ($product->status)
       @if($product->user_id==Auth::id())
<input type = 'checkbox' id ="{{$product->id}}" checked> 
@else 
     <input type = 'hidden' id ="{{$product->id}}" >  @endif
       @else
       <input type = 'checkbox' id ="{{$product->id}}">  
       @endif
                
</td>



</tr>
</tbody>


@endforeach
@can('owner') <a  href="{{route('products.create')}}">Create a new product</a>@endcan('owner')
<script>
        $(document).ready(function(){
            $(":checkbox").click(function(event){
                console.log(event.target.checked);
                $.ajax({
                    url: "{{url('products')}}"+"/"+event.target.id,
                    dataType: 'json',
                    type: 'put',
                    contentType: 'application/json',
                    data: JSON.stringify({ "status": event.target.checked,
                                            _token: '{{csrf_token()}}'}),
                    processData: false,
                    success: function( data, textStatus, jQxhr ){
                         console.log(JSON.stringify( data ));
                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                        console.log( errorThrown );
                    }
                });                
            });
        });
    </script>  
  
@endsection
    
<style>
.rr{
    background-color: green;
}
.rrr{
    background-color: green;
}
table,th,td{
     border: 1px solid black;
     height: 10vh;
      margin: 0;
      
}
.el{
   
    margin-bottom: 10px;
}
.try{
    padding: 0 25px ! important;
    font-size: 50px ! important;
}
th,td{
    padding: 0 25px ! important;
}
.r{
    font-weight:bold ;
}

</style>

