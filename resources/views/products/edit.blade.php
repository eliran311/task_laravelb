@extends('layouts.app')

@section('content')

<form method="post"  action="{{action('ProductController@update', $product->id) }}">
 @method('PATCH')
 @csrf
 <div class="form-group">
   <label for="title">Update product</label>
   <input type="text" class="form-control" name="name" value="{{ $product->name}}" />
   <label for="title">Update price</label>
   <input type="text" class="form-control" name="price" value="{{ $product->price}}" />

 </div>
 <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection